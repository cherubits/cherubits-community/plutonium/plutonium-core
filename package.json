{
  "name": "plutonium-core",
  "version": "0.0.1",
  "description": "Core directives and services of the Plutonium framework",
  "author": "László Hegedűs",
  "contributors": [
    {
      "email": "laszlo.hegedus@cherubits.hu",
      "name": "László Hegedűs",
      "url": "https://github.com/lordoftheflies/"
    }
  ],
  "license": "Apache-2.0",
  "keywords": [
    "webcomponents",
    "material",
    "plutonium",
    "cherubits"
  ],
  "homepage": "https://github.com/lordoftheflies/plutonium-core/",
  "bugs": {
    "url": "https://github.com/lordoftheflies/plutonium-core/issues",
    "email": "support@plutonium.cherubits.hu"
  },
  "bin": {
    "plutonium-cli": "cat"
  },
  "directories": {
    "doc": "./doc",
    "lib": "./lib",
    "test": "./test"
  },
  "main": "index.js",
  "module": "index.js",
  "scripts": {
    "start": "concurrently --kill-others --names tsc,es-dev-server \"npm run tsc:watch\" \"es-dev-server --app-index demo/index.html --node-resolve --open --watch\"",
    "tsc:watch": "tsc --watch",
    "lint:eslint": "eslint --ext .ts,.html . --ignore-path .gitignore",
    "format:eslint": "eslint --ext .ts,.html . --fix --ignore-path .gitignore",
    "lint:prettier": "prettier \"**/*.js\" \"**/*.ts\" --check --ignore-path .gitignore",
    "format:prettier": "prettier \"**/*.js\" \"**/*.ts\" --write --ignore-path .gitignore",
    "lint": "npm run lint:eslint && npm run lint:prettier",
    "format": "npm run format:eslint && npm run format:prettier",
    "test": "tsc && karma start --coverage",
    "test:watch": "concurrently --kill-others --names tsc,karma \"npm run tsc:watch\" \"karma start --auto-watch=true --single-run=false\"",
    "storybook": "concurrently --kill-others --names tsc,storybook \"npm run tsc:watch\" \"start-storybook --node-resolve --watch --open\"",
    "storybook:build": "build-storybook",
    "analyze:elements": "wca analyze src --format json --outFile custom-elements.json",
    "analyze:docs": "wca analyze src --format markdown --outFile README.md",
    "start:backend": "node middleware.js"
  },
  "dependencies": {
    "@open-wc/scoped-elements": "^1.2.1",
    "@sentry/browser": "^5.21.1",
    "@webcomponents/webcomponentsjs": "^2.4.4",
    "app-datepicker": "^4.1.0",
    "lit-element": "^2.2.1",
    "lit-element-router": "^2.0.3",
    "lit-html": "^1.1.2",
    "lit-translate": "^1.2.1",
    "moment": "^2.27.0"
  },
  "devDependencies": {
    "@koa/router": "^9.1.0",
    "@open-wc/demoing-storybook": "^2.0.0",
    "@open-wc/eslint-config": "^2.0.0",
    "@open-wc/testing": "^2.0.0",
    "@open-wc/testing-karma": "^3.0.0",
    "@semantic-release/changelog": "^5.0.1",
    "@semantic-release/commit-analyzer": "^8.0.1",
    "@semantic-release/git": "^9.0.0",
    "@semantic-release/github": "^7.0.7",
    "@semantic-release/gitlab": "^6.0.4",
    "@semantic-release/gitlab-config": "^8.0.0",
    "@semantic-release/npm": "^7.0.5",
    "@semantic-release/release-notes-generator": "^9.0.1",
    "@types/node": "13.11.1",
    "@typescript-eslint/eslint-plugin": "^2.20.0",
    "@typescript-eslint/parser": "^2.20.0",
    "concurrently": "^5.1.0",
    "conventional-changelog-eslint": "^3.0.8",
    "deepmerge": "^3.2.0",
    "es-dev-server": "^1.23.0",
    "eslint": "^6.1.0",
    "eslint-config-prettier": "^6.11.0",
    "husky": "^1.0.0",
    "karma-coverage": "^2.0.3",
    "karma-junit-reporter": "^2.0.1",
    "koa-compose": "^4.1.0",
    "koa-json": "^2.0.2",
    "koa-jsonapi": "^0.0.0",
    "koa-proxies": "^0.11.0",
    "lint-staged": "^10.0.0",
    "prettier": "^2.0.4",
    "semantic-release": "^17.1.1",
    "tslib": "^1.11.0",
    "typescript": "~3.8.2",
    "web-component-analyzer": "^1.1.6"
  },
  "repository": {
    "type": "git",
    "url": "https://github.com/lordoftheflies/plutonium-core.git",
    "directory": "./dist"
  },
  "eslintConfig": {
    "extends": [
      "@open-wc/eslint-config",
      "eslint-config-prettier"
    ]
  },
  "prettier": {
    "singleQuote": true,
    "arrowParens": "avoid"
  },
  "husky": {
    "hooks": {
      "pre-commit": "lint-staged"
    }
  },
  "lint-staged": {
    "*.ts": [
      "eslint --fix",
      "prettier --write",
      "git add"
    ]
  },
  "publishConfig": {
    "registry": "https://registry.npmjs.org/",
    "tag": "latest"
  },
  "config": {
    "backendProtocol": "http",
    "backendHost": "localhost",
    "backendPort": 8001,
    "backendContext": "/api",
    "frontendProtocol": "http",
    "frontendHost": "localhost",
    "frontendPort": 8000,
    "frontendContext": "/"
  },
  "engines": {
    "npm": "~6.14.7",
    "node": ">=10.19.0"
  },
  "os": ["darwin", "linux"],
  "cpu" : ["x64"],
  "dist": {
    "tarball": "./dist"
  }
}
